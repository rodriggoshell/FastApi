from fastapi import FastAPI
from pydantic import BaseModel
from typing import List, Optional
from uuid import uuid4

app = FastAPI()

@app.get("/")
def read_root():
    return 'Ola Thaina'

# Rota com parametro or id o str e para tipar a variaval
# vou passar um parametro viar url
# fica assim localhost:http://127.0.0.1:8000/rota/parametro
# o @app.get('/produtos/{varival}')
# essa varival da rota e vai na url que e o parametro que eu vou colocar
# a segunda varival ela vai receber o valor que vem o do parametro
# entao o valor do id ele vai para essa segunda varivale dentro da def
@app.get("/produtos/{none}")
def read_produtos(none: str):
    text = f'voce comprou {none}'
    return {"mensagem": text}

# rota query
# na tora tem que se passado assim
# http://127.0.0.1:8000/teste?valor=Ana Paula Mesquita
# o valor -> e o valor do parametro que a url vair
# receber
# posso colocar um parametro padrao aqui no back
@app.get('/teste')
def teste(valor: str): #str e uma tipagem, essa varaivel e uma string
    holla = 'holla pra voce' #essa variaval e opcional, mas depois tenho que ver qual a sua real finalidade
    return f'Ola {valor}, {holla}'


class Produto(BaseModel):
    name: str
    valor: float

#-------------------------  UMA OBSERVACAO ROTAS -----------------------------------------
# OBS uma rota com id -> serve para deletar no metodo delete uma coisa espesifica
# Ou o buscar uma coisa espesifica no metodo get
# Eu posso ter ex duas rotas animais com metodos diferentes 
# Ex(@pp.get('/perfil') @app.post('/perfil') @app.put('/perfil{aquivem_id}')@app.delete('/perfil{aquivem_id}'))
# ------------------------ DIVISAO ------------------------------------------------

# rota post -> para mandar informacoes do usuario para o servidor
# um funcao com parametro sem id 
@app.post('/produtos')
def produtos(produto: Produto):
    return f'{produto.name} no valor de {produto.valor}, cadastro em nossa base de dados!'

@app.get('/pedidos')
def read_pedidos():
    pedidos = 'seus pedidos'
    return pedidos

@app.get('/animais')
def listar_animais(animais: str):
    animal = ['gato', 'pato', 'cachorro', 'cavalo', 'boi']
    return f'[{animal}]'

# lista de animais
class Animais(BaseModel):
    id:Optional[str]
    nome: str
    idade:int
    sexo: str
    cor: str

# pode exister uma rota com o mesmo nome para mais de uma funcao
# nesse caso passamos id, para pegar um animal em espesifico
@app.get('/animais/{animal_id}')
def obter_animal(animal_id: str):
    for animal in banco:
        if animal.id == animal_id:
            return animal
    try:
        print('cadastrado')
    except Error:
        print('Falha', Error)
    return animal_id

# rota delete, para deletar e so tem como deletar todo e qualquer
# cadastro em um banco de dados via id
# aqui estou usando o mesmo id da rota get e o mesmo parametro 
@app.delete('/animais/{animal_id}')
def remover_animal(animal_id):
    posicao = -1 #posicao do indice
    for index, animal in enumerate(banco):
        if animal.id == animal_id:
            posicao = index
            break
    if posicao != -1:
        banco.pop(posicao)
        return {'mensage': 'animal removido com sucesso'}
    else:
        return {'mensaga': 'falha no engano'}

banco: List[Animais] = []

# essa rota esta me listando todos os pets que tenho cadastrado em meu banco
@app.get('/pets')
def pets():
    return banco


@app.post('/pets')
def create_pets(animal: Animais):
    animal.id = str(uuid4()) #para pegar pelo id que nesse caso e um has
    banco. append(animal)
    return None
